
/**
* Class Factory
*/
var classFactory = (function (window, $) {

    /**
    * Constructor
    */
    function ClassFactory(fn, config) {
        this.constr = fn;
        this.config = config instanceof Array ? config : [];
        this.addPrototypes();
    }

    /**
    * Static properties and methods
    */

    // Create class
    ClassFactory.createClass = function (fn, config) {
        return new ClassFactory(fn, config).constr;
    };

    // Functions available for creation
    ClassFactory.fns = {};

    // Merge options
    ClassFactory.fns.mergeOptions = function (opts, defaults) {
        this.options = $.extend({}, this.defaults, defaults, this.options, opts);
        return this;
    };

    // Resolve el
    ClassFactory.fns.resolveEl = function () {
        var opts = this.options;
        this.$el = opts.$el || opts.el ? $(opts.el) : $("body");
        this.el = this.$el[0];
        return this;
    };

    // Trigger
    ClassFactory.fns.trigger = function (ev, data, context) {
        var ns = this.ns || "";
        this.$el.trigger.call(context || this, ns + ev, data);
        return this;
    };

    /**
    * Instance properties and methods
    */

    // Resolve el
    ClassFactory.prototype.resolveEl = ClassFactory.fns.resolveEl;

    // Trigger
    ClassFactory.prototype.trigger = ClassFactory.fns.trigger;

    // Add prototype fns
    ClassFactory.prototype.addPrototypes = function () {
        var proto = this.constr.prototype,
            fns = ClassFactory.fns,
            config = this.config,
            i = config.length,
            fnName;
        for (i; i; i -= 1) {
            fnName = config[i - 1];
            if (fns[fnName]) {
                proto[fnName] = fns[fnName];
            }
        }
        return this;
    };

    // Expose
    return ClassFactory.createClass;
}(window, jQuery));
